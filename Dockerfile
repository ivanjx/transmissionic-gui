FROM alpine:latest AS downloader
ARG HASH=a728b5333be6cb06b6adaf9cfc331ed937953302
RUN apk add unzip wget
RUN wget https://github.com/6c65726f79/Transmissionic/archive/$HASH.zip
RUN unzip $HASH.zip && \
    mv /Transmissionic-$HASH /download

FROM node:16 as builder
COPY --from=downloader ./download /transmissionic
WORKDIR /transmissionic
RUN npm install --loglevel verbose
RUN npm run build:webui

FROM nginx:1.25.1-alpine-slim
COPY --from=builder /transmissionic/dist /usr/share/nginx/html
COPY ./default.json /usr/share/nginx/html/
